package pl.qalabs.blog.junit5.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;


class SeleniumChromeTest {

    private WebDriver driver;
    private static final Logger LOG = LoggerFactory.getLogger(SeleniumChromeTest.class);

    @BeforeAll
    static void setupDriver() {
        LOG.info("Setting up Chrome driver with WebDriverManager");
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Make sure that you have newest Chrome installed. The driver is managed by <code>{@link WebDriverManager}</code>.
     */
    @BeforeEach
    void chromeDefault() {
        LOG.info("Starting Chrome driver");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        driver = new ChromeDriver(options);
    }

    @AfterEach
    void tearDown() {
        LOG.info("Quitting Chrome driver");
        driver.quit();
    }

    @Test
    void mainPage() throws InterruptedException {
        driver.get("http://195.150.194.189:8080");

        assertAll(
            () -> assertEquals("perun-ui", driver.getTitle()),
            () -> assertNotNull(driver.findElement(By.xpath("/html/body/div[2]/div[1]/div/div/header")).getText())

        );
        Thread.sleep(5000);
    }

    @Test
    public void introductionText() throws Exception {
        driver.get("http://195.150.194.189:8080/#/industry-login");

        WebElement login = driver.findElement(By.xpath("/html/body/div[1]/div[2]/form/div/div/fieldset[1]/div/input"));
        WebElement password = driver.findElement(By.xpath("/html/body/div[1]/div[2]/form/div/div/fieldset[2]/div/input"));
        login.sendKeys("VALEO");
        password.sendKeys("test");
        WebElement button_login = driver.findElement(By.xpath("/html/body/div[1]/div[2]/form/div/div/button"));
        button_login.click();
        Thread.sleep(5000);
    }
}
